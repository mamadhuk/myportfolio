const async       = require('async');
const express     = require('express');
const bodyParser  = require('body-parser');

const app = express();

// Load all nodejs middleware
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({
	extended: true
}));

// app.engine('html', require('ejs').renderFile);

app.use(express.static(__dirname + '/../public/'));

/*Set server port and project path*/
app.set('port', (process.env.PORT || 11111));

function startExpress(connection) {
	var server = app.listen(app.get('port'),function() {
		console.log("Node app is running at localhost:" + app.get('port'))
	});
}

startExpress();