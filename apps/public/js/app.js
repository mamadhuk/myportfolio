var app = angular.module('myPortfolio', ['ngMaterial', 'ngRoute', 'angularMoment', 'ngAnimate', 'ui.router', 'ngMessages','ui.bootstrap']);


app.config(function($uibTooltipProvider) {
    $uibTooltipProvider.options({
        'property' : 'value'
    });
});


app.config(['$httpProvider', '$mdThemingProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider' ,
  function($httpProvider, $mdThemingProvider, $locationProvider, $stateProvider, $urlRouterProvider) {

    const _ = require('lodash')

   $stateProvider.states = function(states, obj) {
      _.forEach(states, function(s) {$stateProvider.state(s,_.clone(obj))})
       return $stateProvider;
   };

  $stateProvider
  .state('home', {
    url : "/home",
    templateUrl: 'pages/home.html',
    controller: 'dashboardCtrl',
  })
  // .states(['admin.notifications', 'dashboard.notifications'], {
  //   url : "/notifications",
  //   resolve: {
  //     authSrv: 'authSrv',
  //     loggedInUser: function(authSrv) { return authSrv.checkAuth() }
  //   },
  //   controller: function($mdDialog, $state, $scope,notificationSrv ){
  //      if (notificationSrv.notifications.length) { 
  //       $mdDialog.show({
  //         parent: angular.element(document.body),
  //         scope: $scope,
  //         clickOutsideToClose: true,
  //         controller: 'notificationCtrl',
  //         templateUrl: 'pages/notifications.html',
  //         onRemoving: function(){
  //           $state.go($state.previous.name);
  //         }
  //       })
  //      } else {
  //         $state.go($state.previous.name);
  //      }
      
  //   }
  // })
  
  // .state('home', {
  //   // var isFirefox = typeof InstallTrigger !== 'undefined';
  //   // var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
  //   // var isChrome = !!window.chrome && !!window.chrome.webstore;

  //   templateUrl: (typeof InstallTrigger !== 'undefined' || /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification) || !!window.chrome && !!window.chrome.webstore) ? "pages/newLanding.html" : "pages/IENotSupported.html",
    
  //   controller: 'loginCtrl'
  // })

  $urlRouterProvider.otherwise(function($injector) {
    var $state = $injector.get('$state');
    $state.go('home');
  });

// Is cross domain required?
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.headers.common = { "Access-Control-Allow-Origin": "*" };
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  
  $mdThemingProvider.theme('default')
  .primaryPalette('light-blue')
  .accentPalette('light-blue');
}])

// app.run(['$animate', function ($animate) {
//     // $log.info('Animations enabled: ', useAnimations);
//     $animate.enabled(true);
// }])
